"use strict";

let request = require("request");
let cheerio = require("cheerio");
let nameList = require("./biblicalNamesList.js");

const TITLEURL = "https://www.blueletterbible.org/study/parallel/paral19.cfm";
const nicknameLength = 32;

exports.startBaptise = (message) => {
    if(message.mentions.users.first() != null) {
      let newName = "";
      let member = message.mentions.members.first();
      message.channel.send(member.displayName + " has lived a sinful life and will be baptised in the Oulujoki to become one with the God again", {code: true})

      //create nickname and change it
      .then((msg) => {
        newName += getName();
        getTitles((title) => {
          newName += ", " + title;
          newName = newName.length > nicknameLength ? newName.substring(0, nicknameLength) : newName;
          member.setNickname(newName)
          .then(() => {
            msg.edit(msg.toString().replace(/`/g, "") + "\n\nYou are now reborn as: " + newName, {code: true});
          })
          .catch((err) => {
            console.log("baptising went wrong:\n" + err);
            msg.edit(msg.toString().replace(/`/g, "") + "\n\nHe drowned in the Oulujoki", {code: true});
          });

        });

      })
      //on error abort
      .catch((err) => {
          console.log(err);
      });
    }
}

//get titles from one site
const getTitles = (cb) => {
  request(TITLEURL, (err, res, body) => {
    if(err) {
      console.log(err);
      return "";
    }
    let titleList = [];
    let $ = cheerio.load(body);
    //get the titles
    $("td.label--half").map((i, e) => {
      if($(e).children().length == 0) {
        titleList.push($(e).text().trim());
      }
    });
    cb(titleList[randomRange(0, titleList.length)]);
  });
  return;
}

//get name from the list
const getName = () => {
  return nameList.getRandomName();
}

//good random
const randomRange = (min = 0, max = 1) => {
  if(min > max) {
    temp = min;
    min = max;
    max = temp;
  }
  return Math.floor(Math.random() * (max-min)) + min;
}
