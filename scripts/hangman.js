"use strict";

let request = require("request");
let cheerio = require("cheerio");

//list of all active games
let games = [];
let starting = false;

//game class
class Game {
  constructor(word, definition, message) {
    this.word = word;
    this.definition = definition;
    this.message = message;
    this.channel = message.channel;
    this.guessed = [" "];
    this.missed = [];
    this.tries = 10;
  }
  //returns the string that is shown to playes with spaces and hidden letters
  currentWord() {
    let curWord = "";
    for(let i=0; i < this.word.length; i++) {
      if(this.guessed.indexOf(this.word[i].toLowerCase()) != -1) {
        curWord += this.word[i] + " ";
      } else {
        curWord += "_ ";
      }
    }
    return curWord;
  }

  //player's guess
  guess(char) {
    //check if char has been guessed already
    if(this.guessed.indexOf(char) == -1 && this.missed.indexOf(char) == -1) {
      //check if guess is correct or false
      if(this.word.toLowerCase().indexOf(char) != -1) {
        this.guessed.push(char);
        //win condition
        if(this.checkWin()) {
          console.log("win");
          return "win";
        }
        console.log("hit");
        return "hit";

      //guess was false
      } else {
        this.missed.push(char);
        this.tries--;
        console.log("missed " + this.tries);
        //lose condition
        if(this.checkLose()) {
          console.log("lose");
          return "lose";
        }
        console.log("miss");
        return "miss";
      }
    } else {
      console.log("duplicate");
      return "duplicate";
    }
  }

  checkWin() {
    let curWord = this.currentWord().replace(/ /g, "");
    console.log(curWord + " " + this.word);
    if(curWord === this.word.replace(/ /g, "")) {
      return true;
    } else {
      return false;
    }
  }

  checkLose() {
    if(this.tries <= 0) {
      return true;
    } else {
      return false;
    }
  }
}

//downloads html from wordgenerator.net and takes the first word
const createWord = (callback) => {

  request({
    uri: "http://www.wordgenerator.net/application/p.php?type=2&id=dictionary_words&spaceflag=false",
  }, (err, res, body) => {
    if(err) console.log(err);
    let $ = cheerio.load(body);
    let word = $("body").text().substring(0, $("body").text().search("Definition:") - 1);
    let definition = $("body").text().substring($("body").text().search("Definition:") + 12, $("body").text().length);
    definition = definition.substring(0, definition.search(","));
    console.log(definition);
    callback(word, definition);
  });
  return;
}

//the actual message that is shown to players, contains basically the interface for players
const updateGameMessage = (game, gameMsg) => {
  let text = game.currentWord();
  let wrongChars = game.missed;
  let metaMessage = "";
  if(gameMsg == "win") {
    metaMessage = "You win!";
    games.splice(games.indexOf(game), 1);
    game.message.react("🎉");
    game.message.react("💯");
    game.channel.send("Definition: " + game.definition, {code: true});
  } else if(gameMsg == "lose") {
    metaMessage = "You lost!";
    games.splice(games.indexOf(game), 1);
    game.message.react("😵");
    game.channel.send("Correct word: " + game.word + "\nDefinition: " + game.definition, {code: true});
  } else if(gameMsg == "hit") {
    metaMessage = "Correct!";
  } else if(gameMsg == "miss") {
    metaMessage = "Miss!";
  } else if(gameMsg == "duplicate") {
    metaMessage = "Duplicate!";
  }
  game.message.edit(text + "\n\n" + wrongChars + "\nTries left: " + game.tries + "\n" + metaMessage, {code: true});
  console.log("games active: " + games.length);
}

//method that finds the game from list using channel as identificator
const findGame = (channel) => {
  for(let i=0; i<games.length; i++) {
    if(channel == games[i].channel) {
      return i;
    }
  }
  return -1;
}

//starts a game of hangman
exports.startGame = (message) => {
  if(starting) {
    starting = false;
    return;
  } else {
    starting = true;
  }
  if(findGame(message.channel) == -1) {
    message.channel.send("Loading...").then((msg) => {
        createWord( (word, desc) => {
          games.push(new Game(word, desc, msg));
          updateGameMessage(games[games.length - 1], "new game");
          starting = false;
        });
    }).catch((err) => {
      message.channel.send("Game crashed: " + err);
      games.splice(findGame(message.channel, 1));
      starting = false;
    });
  } else {
    message.channel.send("Game is already in progress");
  }
}

//hm *letter*, takes player's msg and guesses the letter
exports.guess = (message) => {
  //show help
  if(message.content == "hm help") {
    message.channel.send("Hangman\nPrefix: hm\nGuess letters, for example 'hm a' guesses letter a\nhm exit or hm quit to end the game.", {code: true});
    return;
  }
  //actual game stuff
  let index;
  if((index = findGame(message.channel)) != -1) {
    if(message.content == "hm quit" || message.content == "hm exit") {
      games.splice(index, 1);
      message.channel.send("You aborted hangman.");
      starting = false;
      return;
    }
    let mes = message.content.trim();
    let char = mes[3].toLowerCase();
    if(char == null) {
      message.delete();
      return;
    }
    let gameMessage = games[index].guess(char);
    console.log(message.author.username + " guessed " + char);
    updateGameMessage(games[index], gameMessage);
    message.delete();
  } else {
    message.channel.send("No game is running! Start a game with the command 'tp hangman'");
  }
}
