"use strict";

/* require */
require("dotenv").config();
const Discord = require('discord.js');
const client = new Discord.Client();
const token = process.env.TOKEN;
const hangman = require("./scripts/hangman.js");
const baptise = require("./scripts/baptise.js");
const nextMeme = require("./scripts/nextMeme.js");

/* stuff */
let msgCounter = 1;
let activated = false;
let policeSiren = 0;
//let blocklist = [];
const prefix = "tp ";
const emoji = {
  megusta: "658413442083848214",
  rageface: "658413628403351617"
}


client.on('ready', () => {
    console.log("I am ready");
    client.user.setActivity('Oulu', {type: 'WATCHING'});
});

//tp oulu
client.on('message', message => {
  if (!message.content.startsWith(prefix)) return;
  if(message.content === prefix + "oulu") {
    message.channel.send("\
    ```Oulu is a city in northern finland, about 100000 inhabitants. \
Oulu is another hi-tech region in finland because strong investments to education. \
Oulu is well-known for [pilluralli].\
Icehockey team of Oulu was champion in finnish national league 2005. \
    \nExample(s)\
    \n-Ookkonää Oulusta, pelekääkkönää polliisia?\
    \n-Are you from Oulu, do you fear police?\
    \n(Very old slogan in Oulu dialect) ```\
    ");
  }
});

//tp hangman
client.on('message', message => {
  if (!message.content.startsWith(prefix)) return;
  if(message.content === prefix + "hangman" || message.content === prefix + "hm") {
    hangman.startGame(message);
  }
});

//tp meme
client.on('message', message => {
  if (!message.content.startsWith(prefix)) return;
  if(message.content === prefix + "meme") {
    nextMeme.nextMeme()
    .then(res => {
      const date = new Date(res.timestamp * 1000);
      const embed = {
        image: {url: res.url},
        description: `Posted: ${date.getDate()}.${date.getMonth() + 1}.${date.getFullYear()}`
      }
      message.channel.send({embed: embed})
      .then(mes => mes.react(emoji.rageface))
      .then(reaction => reaction.message.react(emoji.megusta))
      .catch(err => console.log(err));
    })
    .catch(err => {
      message.channel.send("Your mom");
      console.log(err);
    });
  }
});

//tp baptise
client.on('message', message => {
  if (!message.content.startsWith(prefix)) return;
  if(message.content.includes("baptise") || message.content.includes("baptize") || message.content.includes("bap")) {
    baptise.startBaptise(message);
  }
});

//hm (letter)
client.on('message', message => {
  let hangmanPrefix = "hm ";
  if (!message.content.startsWith(hangmanPrefix)) return;
  if(message.content) {
    hangman.guess(message);
  } else {
    //maybe a warning
  }
});

//tp mute @mention
client.on('message', message => {
  if (!message.content.startsWith(prefix)) return;
  if(message.content.includes(prefix + "mute")) {
    let member = message.mentions.members.first();
    if(member !== undefined) {
      if(message.member.hasPermission("MUTE_MEMBERS")) {
        member.setMute(true);
      } else {
        message.channel.send(message.member.displayName + ", you cannot command me plebian");
      }
    } else {
      message.channel.send("You make me angry");
    }
  }
});

//tp unmute @mention
client.on('message', message => {
  if (!message.content.startsWith(prefix)) return;
  if(message.content.includes(prefix + "unmute")) {
    let member = message.mentions.members.first();
    if(member !== undefined) {
      if(message.member.hasPermission("MUTE_MEMBERS")) {
        member.setMute(false);
      } else {
        message.channel.send(message.member.displayName + ", you cannot command me plebian");
      }
    } else {
      message.channel.send("You make me angry");
    }
  }
});

//auto mute
/*
client.on("voiceStateUpdate", (oldMember, newMember) => {
  if(blocklist.includes(oldMember.user)) {
    oldMember.setMute(true);
    console.log("muted");
  }
});
*/

//tp avatar @mention
client.on('message', message => {
  if (!message.content.startsWith(prefix)) return;
  if(message.content.includes(prefix + "avatar")) {
    let user = message.mentions.users.first();
    if(user !== undefined) {
      message.channel.send(user.avatarURL);
    } else {
      message.channel.send("You make me angry");
    }
  }
});

//react to messages
client.on('message', message => {
  if(msgCounter >= 5 && !message.content.startsWith("hm")) {
    message.react("🤔").catch((error) => {
      return;
    });
    message.react("👌").catch((error) => {
      return;
    });
    msgCounter = 1;
  } else {
    msgCounter++;
  }
});


//tp help
client.on('message', message => {
  if (!message.content.startsWith(prefix)) return;
  if(message.content === prefix + "help") {
    message.channel.send("oulu = oulu\n@toripoliisi\nmute/unmute\navatar\n" +
    "hangman = play hangman, prefix in game 'hm'\n" +
    "bap @mention\n" +
    "meme" +
    "Repo: https://bitbucket.org/nikugronberg/toripolliisi/src/master/", {code: true});
  }
});


client.login(token);
